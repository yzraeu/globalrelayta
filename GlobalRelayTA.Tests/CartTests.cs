using GlobalRelayTA.Application.Services;
using GlobalRelayTA.Domain.Entities;
using GlobalRelayTA.Domain.Enum;
using GlobalRelayTA.Domain.Exceptions;
using GlobalRelayTA.Services.ProductSales;
using NUnit.Framework;
using Shouldly;
using System;

namespace Tests
{
	public class CartTests
	{
		// Service
		CartService cartService;

		// sample entities
		Product cheerios, apples, tv, noStockChocolate, onSaleEnergyBar1, onSaleEnergyBar2, onSaleEnergyBar3, onSaleEnergyBar4, onSaleOrange;
		Coupon simplePercentageCoupon, simpleAmountCoupon, thresholdPercentageCoupon, thresholdAmountCoupon, expiredCoupon;

		[SetUp]
		public void Setup()
		{
			var couponService = new CartCouponService();
			var productSaleService = new ProductSaleService();
			cartService = new CartService(couponService, productSaleService);

			SetupProducts();
			SetupCoupons();
		}

		private void SetupCoupons()
		{
			simplePercentageCoupon = new Coupon()
			{
				Code = "091281R",
				Description = "Gives 6% of discount; no other rules",
				CouponType = CouponType.Percentage,
				DiscountAmount = 6,
				ExpirationDate = DateTime.Now.AddDays(1)
			};

			simpleAmountCoupon = new Coupon()
			{
				Code = "091282R",
				Description = "Gives $10 of discount; no other rules",
				CouponType = CouponType.Amount,
				DiscountAmount = 10,
				ExpirationDate = DateTime.Now.AddDays(1)
			};

			thresholdPercentageCoupon = new Coupon()
			{
				Code = "091283R",
				Description = "Gives 7% of discount if threshold of $100 is surpassed",
				CouponType = CouponType.Percentage,
				DiscountAmount = 7,
				ThresholdValue = 100,
				ExpirationDate = DateTime.Now.AddDays(1)
			};

			thresholdAmountCoupon = new Coupon()
			{
				Code = "091284R",
				Description = "Gives $5 of discount if threshold of $90 is surpassed",
				CouponType = CouponType.Amount,
				DiscountAmount = 5,
				ThresholdValue = 90,
				ExpirationDate = DateTime.Now.AddDays(1)
			};

			expiredCoupon = new Coupon()
			{
				Code = "091281R",
				Description = "Expired coupon",
				CouponType = CouponType.Amount,
				DiscountAmount = 6,
				ExpirationDate = DateTime.Now.AddDays(-1)
			};
		}

		private void SetupProducts()
		{
			noStockChocolate = new Product()
			{
				Name = "Chocolate",
				PriceType = PriceType.Unit,
				BasePrice = 3.99M,
				UnitsInStock = 0
			};

			cheerios = new Product()
			{
				Name = "Cheerios",
				PriceType = PriceType.Unit,
				BasePrice = 6.99M,
				UnitsInStock = 100
			};

			tv = new Product()
			{
				Name = "tv",
				PriceType = PriceType.Unit,
				BasePrice = 599.99M,
				UnitsInStock = 10
			};

			apples = new Product()
			{
				Name = "Apples",
				PriceType = PriceType.Weight,
				BasePrice = 2.49M,
				UnitsInStock = 30
			};

			onSaleEnergyBar1 = new Product()
			{
				Name = "Energy Bar 1",
				PriceType = PriceType.Unit,
				BasePrice = 1.49M,
				UnitsInStock = 20,
				ProductSale = new Buy1Get2nd50Percent()
			};

			onSaleEnergyBar2 = new Product()
			{
				Name = "Energy Bar 2",
				PriceType = PriceType.Unit,
				BasePrice = 1.49M,
				UnitsInStock = 20,
				ProductSale = new Buy1Get1FreeProductSale()
			};

			onSaleEnergyBar3 = new Product()
			{
				Name = "Energy Bar 3",
				PriceType = PriceType.Unit,
				BasePrice = 1.49M,
				UnitsInStock = 20,
				ProductSale = new Buy4Get1FreeProductSale()
			};

			onSaleEnergyBar4 = new Product()
			{
				Name = "Energy Bar 4",
				PriceType = PriceType.Unit,
				BasePrice = 1.49M,
				UnitsInStock = 20,
				ProductSale = new Buy2For2ProductSale()
			};

			onSaleOrange = new Product()
			{
				Name = "Oranges",
				PriceType = PriceType.Weight,
				BasePrice = .99M,
				UnitsInStock = 30,
				ProductSale = new Buy5PoundsGet1DolarOffProductSale()
			};
		}

		[Test]
		public void ShouldThrowException_NullCart()
		{
			Cart cart = null;

			Action action = () => { cartService.AddItem(cart, cheerios, 1); };

			action.ShouldThrow<ArgumentNullException>();

			Action action2 = () => { cartService.CalculateTotal(cart); };

			action2.ShouldThrow<ArgumentNullException>();
		}

		[Test]
		public void ShouldThrowException_NullProduct()
		{
			var cart = new Cart();

			Action action = () => { cartService.AddItem(cart, null, 1); };

			action.ShouldThrow<ArgumentNullException>();
		}

		[Test]
		public void ShouldThrowException_ZeroQuantityProduct()
		{
			var cart = new Cart();

			Action action = () => { cartService.AddItem(cart, null, 0); };

			action.ShouldThrow<ArgumentNullException>();
		}

		[Test]
		public void ShouldThrowException_NegativeQuantityProduct()
		{
			var cart = new Cart();

			Action action = () => { cartService.AddItem(cart, tv, -1); };

			action.ShouldThrow<ArgumentOutOfRangeException>();
		}

		[Test]
		public void ShouldThrowException_OutOfStockProduct()
		{
			var cart = new Cart();

			Action action = () => { cartService.AddItem(cart, noStockChocolate, 1); };

			action.ShouldThrow<ProductOutOfStockException>();
		}

		[Test]
		public void ShouldThrowException_NotEnoughStockProduct()
		{
			var cart = new Cart();

			Action action = () => { cartService.AddItem(cart, tv, 11); };

			action.ShouldThrow<ProductOutOfStockException>();
		}

		[Test]
		public void ShouldGetSimpleTotal_OneItem()
		{
			var cart = new Cart();

			cartService.AddItem(cart, cheerios, 1);
			var total = cartService.CalculateTotal(cart);

			total.ShouldBe(6.99M);
		}

		[Test]
		public void ShouldGetSimpleTotal_TwoItems()
		{
			var cart = new Cart();

			cartService.AddItem(cart, cheerios, 1);
			cartService.AddItem(cart, apples, 0.25M);
			var total = cartService.CalculateTotal(cart);

			total.ShouldBe(7.61M);
		}

		[Test]
		public void ShouldGetSimpleTotal_AddRemoveItem()
		{
			var cart = new Cart();

			cartService.AddItem(cart, cheerios, 3);
			cartService.RemoveItem(cart, cheerios, 1);
			cartService.AddItem(cart, cheerios, 1);
			cartService.RemoveItem(cart, cheerios, 3);
			var total = cartService.CalculateTotal(cart);

			total.ShouldBe(0M);
		}

		[Test]
		public void ShouldGetSimpleTotal_AddRemoveWeightItem()
		{
			var cart = new Cart();

			cartService.AddItem(cart, apples, 1.51M);
			cartService.AddItem(cart, apples, 3.51M);
			cartService.RemoveItem(cart, apples, 3);
			var total = cartService.CalculateTotal(cart);

			total.ShouldBe(5.03M);
		}

		[Test]
		public void ShouldThrowException_DecimalValueOnUnitProduct()
		{
			var cart = new Cart();

			Action action = () => { cartService.AddItem(cart, cheerios, 1.51M); };

			action.ShouldThrow<ArgumentOutOfRangeException>();
		}

		[Test]
		public void ShouldThrowException_ExpiredCoupon()
		{
			var cart = new Cart();

			cartService.AddItem(cart, cheerios, 5);
			cartService.AddItem(cart, apples, 0.78M);
			cartService.AddItem(cart, tv, 1);
			var total = cartService.CalculateTotal(cart);

			total.ShouldBe(636.88M);

			Action action = () => { cartService.ApplyCoupon(cart, expiredCoupon); };

			action.ShouldThrow<ExpiredCouponException>();
		}

		[Test]
		public void ShouldGetTotal_PercentageDiscount()
		{
			var cart = new Cart();

			cartService.AddItem(cart, cheerios, 5);
			cartService.AddItem(cart, apples, 0.78M);
			cartService.AddItem(cart, tv, 1);
			var total = cartService.CalculateTotal(cart);

			total.ShouldBe(636.88M);

			cartService.ApplyCoupon(cart, simplePercentageCoupon);
			total = cartService.CalculateTotal(cart);

			total.ShouldBe(598.67M);
		}

		[Test]
		public void ShouldGetTotal_AmountDiscount()
		{
			var cart = new Cart();

			cartService.AddItem(cart, cheerios, 5);
			cartService.AddItem(cart, apples, 0.78M);
			cartService.AddItem(cart, tv, 1);
			var total = cartService.CalculateTotal(cart);

			total.ShouldBe(636.88M);

			cartService.ApplyCoupon(cart, simpleAmountCoupon);
			total = cartService.CalculateTotal(cart);

			total.ShouldBe(626.88M);
		}

		[Test]
		public void ShouldGetTotal_ThresholdPercentageDiscount()
		{
			var cart = new Cart();

			cartService.AddItem(cart, cheerios, 5);
			cartService.AddItem(cart, apples, 0.78M);
			cartService.AddItem(cart, tv, 1);
			var total = cartService.CalculateTotal(cart);

			total.ShouldBe(636.88M);

			cartService.ApplyCoupon(cart, thresholdPercentageCoupon);
			total = cartService.CalculateTotal(cart);

			total.ShouldBe(592.30M);
		}

		[Test]
		public void ShouldGetTotal_ThresholdAmountDiscount()
		{
			var cart = new Cart();

			cartService.AddItem(cart, cheerios, 5);
			cartService.AddItem(cart, apples, 0.78M);
			cartService.AddItem(cart, tv, 1);
			var total = cartService.CalculateTotal(cart);

			total.ShouldBe(636.88M);

			cartService.ApplyCoupon(cart, thresholdAmountCoupon);
			total = cartService.CalculateTotal(cart);

			total.ShouldBe(631.88M);
		}

		[Test]
		[TestCase(1, 1.49)]
		[TestCase(2, 1.49)]
		[TestCase(3, 2.98)]
		[TestCase(4, 2.98)]
		[TestCase(5, 4.47)]
		[TestCase(6, 4.47)]
		[TestCase(7, 5.96)]
		[TestCase(8, 5.96)]
		[TestCase(9, 7.45)]
		[TestCase(10, 7.45)]
		public void ShouldGetTotal_ProductBuy1Get1FreeSale(int quantity, decimal expectedResult)
		{
			var cart = new Cart();

			cartService.AddItem(cart, onSaleEnergyBar2, quantity);
			var total = cartService.CalculateTotal(cart);

			total.ShouldBe(expectedResult);
		}

		[Test]
		[TestCase(1, 1.49)]
		[TestCase(2, 2.24)]
		[TestCase(3, 3.72)]
		[TestCase(4, 4.47)]
		[TestCase(5, 5.96)]
		[TestCase(6, 6.70)]
		[TestCase(7, 8.20)]
		[TestCase(8, 8.94)]
		[TestCase(9, 10.43)]
		[TestCase(10, 11.18)]
		public void ShouldGetTotal_ProductBuy1Get2nd50PercentSale(int quantity, decimal expectedResult)
		{
			var cart = new Cart();

			cartService.AddItem(cart, onSaleEnergyBar1, quantity);
			var total = cartService.CalculateTotal(cart);

			total.ShouldBe(expectedResult);
		}

		[Test]
		[TestCase(1, 1.49)]
		[TestCase(2, 2.98)]
		[TestCase(3, 4.47)]
		[TestCase(4, 4.47)]
		[TestCase(5, 5.96)]
		[TestCase(6, 7.45)]
		[TestCase(7, 8.94)]
		[TestCase(8, 8.94)]
		[TestCase(9, 10.43)]
		[TestCase(10, 11.92)]
		public void ShouldGetTotal_ProductBuy4Get1FreeProductSale(int quantity, decimal expectedResult)
		{
			var cart = new Cart();

			cartService.AddItem(cart, onSaleEnergyBar3, quantity);
			var total = cartService.CalculateTotal(cart);

			total.ShouldBe(expectedResult);
		}

		[Test]
		[TestCase(1, 1.49)]
		[TestCase(2, 2.00)]
		[TestCase(3, 3.49)]
		[TestCase(4, 4.00)]
		[TestCase(5, 5.49)]
		[TestCase(6, 6.00)]
		[TestCase(7, 7.49)]
		[TestCase(8, 8.00)]
		[TestCase(9, 9.49)]
		[TestCase(10, 10.00)]
		public void ShouldGetTotal_ProductBuy2For2ProductSale(int quantity, decimal expectedResult)
		{
			var cart = new Cart();

			cartService.AddItem(cart, onSaleEnergyBar4, quantity);
			var total = cartService.CalculateTotal(cart);

			total.ShouldBe(expectedResult);
		}

		// Weighted discount
		[Test]
		[TestCase(1, 0.99)]
		[TestCase(2, 1.98)]
		[TestCase(3, 2.97)]
		[TestCase(4, 3.96)]
		[TestCase(5, 3.95)]
		[TestCase(6, 4.94)]
		[TestCase(7, 5.93)]
		[TestCase(8, 6.92)]
		[TestCase(9, 7.91)]
		[TestCase(10, 8.90)]
		public void ShouldGetTotal_Buy5PoundsGet1DolarOffProductSale(int quantity, decimal expectedResult)
		{
			var cart = new Cart();

			cartService.AddItem(cart, onSaleOrange, quantity);
			var total = cartService.CalculateTotal(cart);

			total.ShouldBe(expectedResult);
		}

		// Multiple discount
		[Test]
		[TestCase(1, 2.98)]
		[TestCase(2, 3.49)]
		[TestCase(3, 6.47)]
		[TestCase(4, 6.98)]
		[TestCase(5, 9.96)]
		[TestCase(6, 10.47)]
		[TestCase(7, 13.45)]
		[TestCase(8, 13.96)]
		[TestCase(9, 16.94)]
		[TestCase(10, 17.45)]
		public void ShouldGetTotal_MultipleProductSale_Buy1Get1Free_Buy2For2(int quantity, decimal expectedResult)
		{
			var cart = new Cart();

			cartService.AddItem(cart, onSaleEnergyBar2, quantity); // Buy1Get1Free
			cartService.AddItem(cart, onSaleEnergyBar4, quantity); // Buy2For2
			var total = cartService.CalculateTotal(cart);

			total.ShouldBe(expectedResult);
		}

		[Test]
		public void ShouldGetTotal_ProductBuy1Get1Sale_SimplePercentegaDiscount()
		{
			var cart = new Cart();

			cartService.AddItem(cart, onSaleEnergyBar2, 9);
			cartService.ApplyCoupon(cart, simplePercentageCoupon);
			var total = cartService.CalculateTotal(cart);

			total.ShouldBe(7.00M);
		}
	}
}