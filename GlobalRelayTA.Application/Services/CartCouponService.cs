﻿using GlobalRelayTA.Application.Interfaces;
using GlobalRelayTA.Domain.Entities;
using GlobalRelayTA.Domain.Enum;

namespace GlobalRelayTA.Application.Services
{
	public class CartCouponService : ICartCouponService
	{
		public DiscountCartItem Process(Coupon coupon, decimal cartTotal)
		{
			if (coupon == null)
				return null;

			if ((coupon.CouponType == CouponType.Amount) || (coupon.CouponType == CouponType.ThresholdAmount && cartTotal >= coupon.ThresholdValue))
			{
				return new DiscountCartItem(coupon.CouponType.ToString(), coupon.DiscountAmount);

			}
			else if ((coupon.CouponType == CouponType.Percentage) || (coupon.CouponType == CouponType.ThresholdAmount && cartTotal >= coupon.ThresholdValue))
			{
				var dicountAmount = (cartTotal * coupon.DiscountAmount) / 100;
				return new DiscountCartItem(coupon.CouponType.ToString(), dicountAmount);
			}

			return null;
		}
	}
}
