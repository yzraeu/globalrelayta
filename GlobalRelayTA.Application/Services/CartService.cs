﻿using GlobalRelayTA.Application.Interfaces;
using GlobalRelayTA.Domain.Entities;
using GlobalRelayTA.Domain.Enum;
using GlobalRelayTA.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GlobalRelayTA.Application.Services
{
	public class CartService : ICartService
	{
		protected readonly ICartCouponService _cartCouponService;
		protected readonly ICartProductSaleService _cartProducSaleService;
		public CartService(
			ICartCouponService cartCouponService,
			ICartProductSaleService cartProducSaleService
			)
		{
			_cartCouponService = cartCouponService;
			_cartProducSaleService = cartProducSaleService;
		}

		private void AddRemoveParameterValidation(Cart cart, Product product, decimal quantity)
		{
			if (cart == null) throw new ArgumentNullException(nameof(cart));
			if (product == null) throw new ArgumentNullException(nameof(product));
			if (quantity < 0) throw new ArgumentOutOfRangeException(nameof(quantity));
			if (product.PriceType == PriceType.Unit && quantity % 1 > 0) throw new ArgumentOutOfRangeException(nameof(quantity));
			if (quantity > product.UnitsInStock) throw new ProductOutOfStockException(product);
		}

		public void AddItem(Cart cart, Product product, decimal quantity)
		{
			AddRemoveParameterValidation(cart, product, quantity);

			if (product.PriceType == PriceType.Unit)
			{
				for (int i = 0; i < quantity; i++)
				{
					var item = new CartItem(product, product.BasePrice, 1);

					cart.CartItems.Add(item);
				}
			}
			else
			{
				var item = new CartItem(product, product.BasePrice, quantity);

				cart.CartItems.Add(item);
			}
		}

		public void RemoveItem(Cart cart, Product product, decimal quantity)
		{
			AddRemoveParameterValidation(cart, product, quantity);

			List<CartItem> filtered = cart.CartItems.Where(ci => ci.Product.Id == product.Id).ToList();

			foreach (var item in filtered)
			{
				if (product.PriceType == PriceType.Unit)
					cart.CartItems.Remove(item); // Just remove it from the collection
				else
				{
					if (item.Quantity > quantity)
						item.Quantity -= quantity; // Subtracts the amount being removed
					else if (item.Quantity <= quantity)
					{
						cart.CartItems.Remove(item); // Remove it from the collection and subtracts the current item amount from the amount that should be removed
						quantity -= item.Quantity;
					}
				}
			}
		}

		public void ApplyCoupon(Cart cart, Coupon coupon)
		{
			if (cart == null) throw new ArgumentNullException(nameof(cart));
			if (coupon == null) throw new ArgumentNullException(nameof(coupon));

			cart.ApplyCoupon(coupon);
		}

		public decimal CalculateTotal(Cart cart)
		{
			CalculateTotalValidation(cart);

			var saleDiscount = _cartProducSaleService.Process(cart);
			if (saleDiscount != null)
				foreach (var item in saleDiscount)
					cart.CartItems.Add(item);

			var couponDiscount = _cartCouponService.Process(cart.Coupon, cart.GetTotal());
			if (couponDiscount != null)
				cart.CartItems.Add(couponDiscount);

			return Math.Round(cart.GetTotal(), 2);
		}

		private void CalculateTotalValidation(Cart cart)
		{
			if (cart == null) throw new ArgumentNullException(nameof(cart));
		}
	}
}
