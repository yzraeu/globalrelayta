﻿using GlobalRelayTA.Application.Interfaces;
using GlobalRelayTA.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GlobalRelayTA.Application.Services
{
	public class ProductSaleService : ICartProductSaleService
	{
		public ICollection<DiscountCartItem> Process(Cart cart)
		{
			if (cart == null) throw new ArgumentNullException(nameof(cart));

			// check for product on sale
			var productsOnSale = new List<Product>(cart.CartItems.Where(ci => ci.Product.OnSale).Select(ci => ci.Product).Distinct());

			// process sales
			var output = new List<DiscountCartItem>();
			foreach (var item in productsOnSale)
			{
				output.AddRange(item.ProductSale.Execute(item, cart.CartItems));
			}

			return output;
		}
	}
}
