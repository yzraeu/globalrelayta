﻿using GlobalRelayTA.Domain.Entities;
using GlobalRelayTA.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GlobalRelayTA.Services.ProductSales
{
	public class Buy4Get1FreeProductSale : BaseProductSale, IProductSale
	{
		public ICollection<DiscountCartItem> Execute(Product product, ICollection<CartItem> cartIems)
		{
			decimal currentItemCount = GetItemCount(product, cartIems);

			var output = new List<DiscountCartItem>();

			if (currentItemCount <= 3) return output;

			var totalDiscount = (Math.Floor(currentItemCount / 4) * product.BasePrice);
			output.Add(new DiscountCartItem($"Buy4Get1Free - {product.Name}", totalDiscount));

			return output;
		}
	}
}
