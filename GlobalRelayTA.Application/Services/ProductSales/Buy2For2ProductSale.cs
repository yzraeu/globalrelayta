﻿using GlobalRelayTA.Domain.Entities;
using GlobalRelayTA.Domain.Interfaces;
using System;
using System.Collections.Generic;

namespace GlobalRelayTA.Services.ProductSales
{
	public class Buy2For2ProductSale : BaseProductSale, IProductSale
	{
		public ICollection<DiscountCartItem> Execute(Product product, ICollection<CartItem> cartIems)
		{
			decimal currentItemCount = GetItemCount(product, cartIems);

			var output = new List<DiscountCartItem>();

			if (currentItemCount <= 1) return output;

			var finalDiscountPrice = 2;

			for (int i = 0; i < Math.Floor(currentItemCount / 2); i++)
			{
				var totalDiscount = (product.BasePrice * 2) - finalDiscountPrice;
				output.Add(new DiscountCartItem($"Buy2For2 - {product.Name}", totalDiscount));
			}

			return output;
		}
	}
}
