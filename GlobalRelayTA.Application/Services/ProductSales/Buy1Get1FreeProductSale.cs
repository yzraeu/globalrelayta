﻿using GlobalRelayTA.Domain.Entities;
using GlobalRelayTA.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GlobalRelayTA.Services.ProductSales
{
	public class Buy1Get1FreeProductSale : BaseProductSale, IProductSale
	{
		public ICollection<DiscountCartItem> Execute(Product product, ICollection<CartItem> cartIems)
		{
			decimal currentItemCount = GetItemCount(product, cartIems);

			var output = new List<DiscountCartItem>();

			if (currentItemCount <= 1) return output;

			var totalDiscount = (Math.Floor(currentItemCount / 2) * product.BasePrice);
			output.Add(new DiscountCartItem($"Buy1Get1Free - {product.Name}", totalDiscount));

			return output;
		}
	}
}
