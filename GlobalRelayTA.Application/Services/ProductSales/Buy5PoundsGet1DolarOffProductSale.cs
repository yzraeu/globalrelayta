﻿using GlobalRelayTA.Domain.Entities;
using GlobalRelayTA.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GlobalRelayTA.Services.ProductSales
{
	public class Buy5PoundsGet1DolarOffProductSale : BaseProductSale, IProductSale
	{
		public ICollection<DiscountCartItem> Execute(Product product, ICollection<CartItem> cartIems)
		{
			decimal currentItemWeight = GetItemWeight(product, cartIems);

			var output = new List<DiscountCartItem>();

			if (currentItemWeight < 5) return output;

			var totalDiscount = 1;
			output.Add(new DiscountCartItem($"Buy5PoundsGet1DolarOff - {product.Name}", totalDiscount));


			return output;
		}
	}
}
