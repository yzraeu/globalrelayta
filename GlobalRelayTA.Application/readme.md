﻿# Application Layer

This layer is the one that should be requested by any UI. It holds the business logic.

In this point it should connect to the Database or with a Persistence/Repository Layer to store data.

I've created every bulk sale discount as class to make a clear separation of concerns and a easy way to update current rules and create new ones.