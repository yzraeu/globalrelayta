﻿using GlobalRelayTA.Domain.Entities;

namespace GlobalRelayTA.Application.Interfaces
{
	public interface ICartCouponService
	{
		DiscountCartItem Process(Coupon coupon, decimal cartTotal);
	}
}
