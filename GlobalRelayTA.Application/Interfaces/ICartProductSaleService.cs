﻿using GlobalRelayTA.Domain.Entities;
using System.Collections.Generic;

namespace GlobalRelayTA.Application.Interfaces
{
	public interface ICartProductSaleService
	{
		ICollection<DiscountCartItem> Process(Cart cart);

	}
}
