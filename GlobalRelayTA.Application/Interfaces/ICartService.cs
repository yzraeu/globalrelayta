﻿using GlobalRelayTA.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace GlobalRelayTA.Application.Interfaces
{
	public interface ICartService
	{
		void AddItem(Cart cart, Product product, decimal quantity);

		void RemoveItem(Cart cart, Product product, decimal quantity);

		decimal CalculateTotal(Cart cart);

		void ApplyCoupon(Cart cart, Coupon coupon);

	}
}
