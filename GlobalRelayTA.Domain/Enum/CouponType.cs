﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GlobalRelayTA.Domain.Enum
{
    public enum CouponType
    {
        Percentage,
        Amount,
        ThresholdPercentage,
        ThresholdAmount
    }
}
