﻿using GlobalRelayTA.Domain.Entities;
using System.Collections.Generic;

namespace GlobalRelayTA.Domain.Interfaces
{
	public interface IProductSale
	{
		ICollection<DiscountCartItem> Execute(Product product, ICollection<CartItem> cartIems);
	}
}
