﻿using GlobalRelayTA.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GlobalRelayTA.Domain.Entities
{
	public class Cart : BaseEntity
	{
		public Cart()
		{
			CartItems = new List<CartItem>();
		}

		// Props
		public decimal GetTotal()
		{
			var cartTotal = 0M;
			foreach (var item in this.CartItems)
			{
				switch (item)
				{
					case DiscountCartItem discountCartItem:
						cartTotal -= discountCartItem.Price;
						break;
					case CartItem cartItem:
						cartTotal += cartItem.Price * cartItem.Quantity;
						break;
				}
			}

			return cartTotal;
		}

		// Relations
		public Coupon Coupon { get; internal set; }

		public void ApplyCoupon(Coupon coupon)
		{
			if (coupon == null) throw new ArgumentNullException(nameof(coupon));
			if (coupon.ExpirationDate < DateTime.Now) throw new ExpiredCouponException(coupon);

			Coupon = coupon;
		}

		// Collections
		public ICollection<CartItem> CartItems { get; private set; }



	}
}
