﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GlobalRelayTA.Domain.Entities
{
	public class CartItem : BaseEntity
	{
		public CartItem()
		{

		}

		public CartItem(Product product, decimal price, decimal quantity)
		{
			this.Product = product;
			this.Price = price;
			this.Quantity = quantity;
		}

		public decimal Quantity { get; set; }
		public decimal Price { get; set; }

		// Relations
		public Cart Cart { get; set; }
		public Product Product { get; set; }
	}
}
