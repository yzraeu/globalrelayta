﻿using GlobalRelayTA.Domain.Enum;
using System;

namespace GlobalRelayTA.Domain.Entities
{
	public class Coupon : BaseEntity
	{
		public string Description { get; set; }
		public string Code { get; set; }
		public CouponType CouponType { get; set; }
		public decimal DiscountAmount { get; set; }
		public decimal? ThresholdValue { get; set; }
		public DateTime ExpirationDate { get; set; }
	}
}
