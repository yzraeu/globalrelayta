﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GlobalRelayTA.Domain.Entities
{
	public class DiscountCartItem : CartItem
	{
		public DiscountCartItem(string name, decimal price)
		{
			this.Product = new Product()
			{
				Name = name,
				PriceType = Enum.PriceType.Unit,
				BasePrice = price,
				Description = "Discount " + name,
			};
			Price = price;
			Quantity = 1;
		}
	}
}
