﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GlobalRelayTA.Domain.Entities
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateUpdated { get; set; }
        public override string ToString()
        {
            return GetType().Name + $" [Id={Id.ToString()}; DateUpdated={DateUpdated.ToString("yyyy-MM-dd HH:mm:ss")}; DateAdded={DateAdded.ToString("yyyy-MM-dd HH:mm:ss")}]";
        }

		public BaseEntity()
        {
            Id = Guid.NewGuid();
			DateAdded = DateUpdated = DateTime.Now;
        }
    }
}
