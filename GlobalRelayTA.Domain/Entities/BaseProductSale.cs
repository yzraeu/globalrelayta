﻿using GlobalRelayTA.Domain.Enum;
using System.Collections.Generic;
using System.Linq;

namespace GlobalRelayTA.Domain.Entities
{
	public class BaseProductSale : BaseEntity
	{
		protected decimal GetItemCount(Product product, ICollection<CartItem> cartIems)
		{
			return FilterCartItems(product, cartIems, PriceType.Unit).Sum(c => c.Quantity);
		}

		protected decimal GetItemWeight(Product product, ICollection<CartItem> cartIems)
		{
			return FilterCartItems(product, cartIems, PriceType.Weight).Sum(c => c.Quantity);
		}

		private IEnumerable<CartItem> FilterCartItems(Product product, ICollection<CartItem> cartIems, PriceType priceType)
		{
			return cartIems.Where(ci => ci.Product.Id == product.Id && ci.Product.PriceType == priceType);
		}
	}
}
