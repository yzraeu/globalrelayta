﻿using GlobalRelayTA.Domain.Enum;
using GlobalRelayTA.Domain.Interfaces;

namespace GlobalRelayTA.Domain.Entities
{
	public class Product : BaseEntity
	{
		public string Name { get; set; }
		public string SKU { get; set; }
		public string Description { get; set; }
		public decimal BasePrice { get; set; }
		public PriceType PriceType { get; set; }
		public int? UnitsInStock { get; set; }
		public bool OnSale { get { return ProductSale != null; } }

		// Relations
		public IProductSale ProductSale { get; set; }
	}
}
