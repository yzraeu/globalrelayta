﻿using GlobalRelayTA.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace GlobalRelayTA.Domain.Exceptions
{
    public class EntityNotRemovedException<T> : Exception where T : BaseEntity
    {
        public EntityNotRemovedException(Guid id) : base($"Entity '{typeof(T).FullName}' with id '{id}' was not removed")
        {
        }
    }
}
