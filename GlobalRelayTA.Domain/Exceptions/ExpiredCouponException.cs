﻿using GlobalRelayTA.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace GlobalRelayTA.Domain.Exceptions
{
    public class ExpiredCouponException : Exception 
    {
        public ExpiredCouponException(Coupon coupon) : base($"Coupon '{coupon.Code}' is expired")
        {
        }
    }
}
