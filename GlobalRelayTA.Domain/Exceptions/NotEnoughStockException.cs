﻿using GlobalRelayTA.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace GlobalRelayTA.Domain.Exceptions
{
    public class NotEnoughStockException : Exception 
    {
        public NotEnoughStockException(Product product, decimal quantity) : base($"Not enough stock for '{product.Name}'-'{product.SKU}'. Client ordered '{quantity}'")
        {
        }
    }
}
