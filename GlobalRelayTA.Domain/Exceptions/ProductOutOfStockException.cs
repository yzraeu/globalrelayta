﻿using GlobalRelayTA.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace GlobalRelayTA.Domain.Exceptions
{
    public class ProductOutOfStockException : Exception 
    {
        public ProductOutOfStockException(Product product) : base($"Product '{product.Name}'-'{product.SKU}' out of stock")
        {
        }
    }
}
