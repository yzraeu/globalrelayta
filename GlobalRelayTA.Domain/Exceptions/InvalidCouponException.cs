﻿using GlobalRelayTA.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace GlobalRelayTA.Domain.Exceptions
{
    public class InvalidCouponException : Exception 
    {
        public InvalidCouponException() : base($"Coupon is invalid")
        {
        }
    }
}
